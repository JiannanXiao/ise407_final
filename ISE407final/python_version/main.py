import googlemaps
from datetime import datetime

gmaps = googlemaps.Client(key='AIzaSyD34ORMdOkdZ-Vkxfyb78kby2jkgFLVsc8')

# # Geocoding an address 1
# geocode_result = gmaps.geocode('1600 Amphitheatre Parkway, Mountain View, CA')
# print(geocode_result)
# # Look up an address with reverse geocoding
# # reverse_geocode_result = gmaps.reverse_geocode((40.714224, -73.961452))
#
# Request directions via public transit
now = datetime.now()
directions_result = gmaps.directions("Lehigh University",
                                     "New York City",
                                     mode="transit",
                                     departure_time=now)

print(directions_result)